package dnd.dndproject.service;

import dnd.dndproject.dto.CampaignDTO;
import dnd.dndproject.exception.CampaignNotFoundException;

import java.util.List;

public interface CampaignService {
    List<CampaignDTO> getAllCampaigns();

    CampaignDTO getCampaignById(Long id) throws CampaignNotFoundException;

    CampaignDTO saveCampaign(CampaignDTO campaignDTO) throws CampaignNotFoundException;

    CampaignDTO deleteCampaign(Long id) throws CampaignNotFoundException;

}

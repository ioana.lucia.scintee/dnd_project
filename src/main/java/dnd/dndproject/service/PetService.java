package dnd.dndproject.service;

import dnd.dndproject.dto.PetDTO;
import dnd.dndproject.exception.PetNotFoundException;

import java.util.List;

public interface PetService {
    List<PetDTO> getAllPets();

    PetDTO getPetById(Long id) throws PetNotFoundException;

    PetDTO savePet(PetDTO petDTO) throws PetNotFoundException;

    PetDTO deletePet(Long id) throws PetNotFoundException;

}

package dnd.dndproject.service;

import dnd.dndproject.dto.AbilityDTO;
import dnd.dndproject.exception.AbilityNotFoundException;

import java.util.List;

public interface AbilityService {

    List<AbilityDTO> getAllAbilities();

    AbilityDTO getAbilityById(Long id) throws AbilityNotFoundException;

    AbilityDTO saveAbility(AbilityDTO abilityDTO) throws AbilityNotFoundException;

    AbilityDTO deleteAbility(Long id) throws AbilityNotFoundException;

}


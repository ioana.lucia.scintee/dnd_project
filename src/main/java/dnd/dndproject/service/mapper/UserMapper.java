package dnd.dndproject.service.mapper;

import dnd.dndproject.dto.UserCreateDTO;
import dnd.dndproject.dto.UserDTO;
import dnd.dndproject.entity.AppUser;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public AppUser userToEntity(UserCreateDTO createDTO) {
        return AppUser.builder()
                .userName(createDTO.getUserName())
                .password(createDTO.getPassword())
                .build();
    }

    public UserDTO userToDTO(AppUser entity) {
        return UserDTO.builder()
                .id(entity.getId())
                .userName(entity.getUserName())
                .build();
    }
}

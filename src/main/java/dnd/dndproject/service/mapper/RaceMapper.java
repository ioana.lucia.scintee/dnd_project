package dnd.dndproject.service.mapper;

import dnd.dndproject.dto.RaceDTO;
import dnd.dndproject.entity.Race;
import org.springframework.stereotype.Component;

@Component
public class RaceMapper {
    public Race raceToEntity(RaceDTO createDTO) {
        return Race.builder()
                .name(createDTO.getName())
                .build();
    }

    public RaceDTO raceToDTO(Race entity) {
        return RaceDTO.builder()
                .name(entity.getName())
                .build();
    }
}

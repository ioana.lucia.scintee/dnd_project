package dnd.dndproject.service.mapper;

import dnd.dndproject.dto.CharacterDTO;
import dnd.dndproject.entity.Character;
import org.springframework.stereotype.Component;

@Component
public class CharacterMapper {

    public Character characterToEntity(CharacterDTO createDTO) {
        return Character.builder()
                .name(createDTO.getName())
                .backstory(createDTO.getBackstory())
                .build();
    }

    public CharacterDTO characterToDTO(Character entity) {
        return CharacterDTO.builder()
                .name(entity.getName())
                .backstory(entity.getBackstory())
                .build();
    }
}

package dnd.dndproject.service.mapper;

import dnd.dndproject.dto.AbilityDTO;
import dnd.dndproject.entity.Ability;
import org.springframework.stereotype.Component;

@Component
public class AbilityMapper {

    public Ability abilityToEntity(AbilityDTO createDTO) {
        return Ability.builder()
                .build();
    }

    public AbilityDTO abilityToDTO(Ability entity) {
        return AbilityDTO.builder()
                .build();
    }
}

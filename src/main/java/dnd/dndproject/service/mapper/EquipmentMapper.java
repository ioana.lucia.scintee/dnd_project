package dnd.dndproject.service.mapper;

import dnd.dndproject.dto.EquipmentDTO;
import dnd.dndproject.entity.Equipment;
import org.springframework.stereotype.Component;

@Component
public class EquipmentMapper {

    public Equipment equipmentToEntity(EquipmentDTO createDTO) {
        return Equipment.builder()
                .name(createDTO.getName())
                .damagePoints(createDTO.getDamagePoints())
                .buff(createDTO.isBuff())
                .debuff(createDTO.isDebuff())
                .buffDuration(createDTO.getBuffDuration())
                .debuffDuration(createDTO.getDebuffDuration())
                .build();
    }

    public EquipmentDTO equipmentToDTO(Equipment entity) {
        return EquipmentDTO.builder()
                .name(entity.getName())
                .damagePoints(entity.getDamagePoints())
                .buff(entity.isBuff())
                .debuff(entity.isDebuff())
                .buffDuration(entity.getBuffDuration())
                .debuffDuration(entity.getDebuffDuration())
                .build();
    }
}

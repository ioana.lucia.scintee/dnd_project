package dnd.dndproject.service.mapper;


import dnd.dndproject.dto.PetDTO;
import dnd.dndproject.entity.Pet;
import org.springframework.stereotype.Component;

@Component
public class PetMapper {
    public Pet petToEntity(PetDTO createDTO) {
        return Pet.builder()
                .name(createDTO.getName())
                .build();
    }

    public PetDTO petToDTO(Pet entity) {
        return PetDTO.builder()
                .name(entity.getName())
                .build();
    }
}

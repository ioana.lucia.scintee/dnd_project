package dnd.dndproject.service.mapper;

import dnd.dndproject.dto.CharacterDetailsDTO;
import dnd.dndproject.entity.CharacterDetails;
import org.springframework.stereotype.Component;

@Component
public class CharacterDetailsMapper {
    public CharacterDetails detailsToEntity(CharacterDetailsDTO createDTO) {
        return CharacterDetails.builder()
                .alignament(createDTO.getAlignament())
                .faith(createDTO.getFaith())
                .build();

    }

    public CharacterDetailsDTO detailsToDTO(CharacterDetails entity) {
        return CharacterDetailsDTO.builder()
                .alignament(entity.getAlignament())
                .faith(entity.getFaith())
                .build();
    }
}

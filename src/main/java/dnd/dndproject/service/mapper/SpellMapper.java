package dnd.dndproject.service.mapper;

import dnd.dndproject.dto.SpellDTO;
import dnd.dndproject.entity.Spell;
import org.springframework.stereotype.Component;

@Component
public class SpellMapper {
    public Spell spellToEntity(SpellDTO createDTO) {
        return Spell.builder()
                .name(createDTO.getName())
                .level(createDTO.getLevel())
                .castingTime(createDTO.getCastingTime())
                .range(createDTO.getRange())
                .damagePoints(createDTO.getDamagePoints())
                .buff(createDTO.isBuff())
                .debuff(createDTO.isDebuff())
                .buffDuration(createDTO.getBuffDuration())
                .debuffDuration(createDTO.getDebuffDuration())
                .elementType(createDTO.getElementType())
                .description(createDTO.getDescription())
                .build();
    }

    public SpellDTO spellToDTO(Spell entity) {
        return SpellDTO.builder()
                .name(entity.getName())
                .level(entity.getLevel())
                .castingTime(entity.getCastingTime())
                .range(entity.getRange())
                .damagePoints(entity.getDamagePoints())
                .buff(entity.isBuff())
                .debuff(entity.isDebuff())
                .buffDuration(entity.getBuffDuration())
                .debuffDuration(entity.getDebuffDuration())
                .elementType(entity.getElementType())
                .description(entity.getDescription())
                .build();
    }
}

package dnd.dndproject.service.mapper;

import dnd.dndproject.dto.StatsDTO;
import dnd.dndproject.entity.Stats;
import org.springframework.stereotype.Component;

@Component
public class StatsMapper {
    public Stats statsToEntity(StatsDTO createDTO) {
        return Stats.builder()
                .build();
    }

    public StatsDTO statsToDTO(Stats entity) {
        return StatsDTO.builder()
                .build();
    }
}

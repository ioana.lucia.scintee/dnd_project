package dnd.dndproject.service.mapper;

import dnd.dndproject.dto.CampaignDTO;
import dnd.dndproject.entity.Campaign;
import org.springframework.stereotype.Component;

@Component
public class CampaignMapper {
    public Campaign campaignToEntity(CampaignDTO createDTO) {
        return Campaign.builder()
                .name(createDTO.getName())
                .minLevel(createDTO.getMinLevel())
                .beginDate(createDTO.getBeginDate())
                .endDate(createDTO.getEndDate())
                .build();
    }

    public CampaignDTO campaignToDTO(Campaign entity) {
        return CampaignDTO.builder()
                .name(entity.getName())
                .minLevel(entity.getMinLevel())
                .beginDate(entity.getBeginDate())
                .endDate(entity.getEndDate())
                .build();
    }
}


package dnd.dndproject.service.mapper;

import dnd.dndproject.dto.CharacterClassDTO;
import dnd.dndproject.entity.CharacterClass;
import org.springframework.stereotype.Component;

@Component
public class CharacterClassMapper {
    public CharacterClass classToEntity(CharacterClassDTO createDTO) {
        return CharacterClass.builder()
                .name(createDTO.getName())
                .primaryAbility(createDTO.getPrimaryAbility())
                .build();

    }

    public CharacterClassDTO classToDTO(CharacterClass entity) {
        return CharacterClassDTO.builder()
                .name(entity.getName())
                .primaryAbility(entity.getPrimaryAbility())
                .build();
    }
}

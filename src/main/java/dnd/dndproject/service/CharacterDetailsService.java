package dnd.dndproject.service;

import dnd.dndproject.dto.CharacterDetailsDTO;
import dnd.dndproject.exception.CharacterDetailsNotFoundException;

import java.util.List;

public interface CharacterDetailsService {
    List<CharacterDetailsDTO> getAllDetails();

    CharacterDetailsDTO getDetailById(Long id) throws CharacterDetailsNotFoundException;

    CharacterDetailsDTO saveDetail(CharacterDetailsDTO characterClassDTO) throws CharacterDetailsNotFoundException;

    CharacterDetailsDTO deleteDetail(Long id) throws CharacterDetailsNotFoundException;

}

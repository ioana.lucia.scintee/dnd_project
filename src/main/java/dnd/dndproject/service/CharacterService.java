package dnd.dndproject.service;

import dnd.dndproject.dto.CharacterDTO;
import dnd.dndproject.exception.CharacterNotFoundException;


import java.util.List;

public interface CharacterService {

    List<CharacterDTO> getAllCharacters();

    CharacterDTO getCharacterById(Long id) throws CharacterNotFoundException;

    CharacterDTO saveCharacter(CharacterDTO characterDTO) throws CharacterNotFoundException;

    CharacterDTO deleteCharacter(Long id) throws CharacterNotFoundException;

}

package dnd.dndproject.service;

import dnd.dndproject.dto.EquipmentDTO;
import dnd.dndproject.exception.EquipmentNotFoundException;

import java.util.List;

public interface EquipmentService {
    List<EquipmentDTO> getAllEquipment();

    EquipmentDTO getEquipmentById(Long id) throws EquipmentNotFoundException;

    EquipmentDTO saveEquipment(EquipmentDTO equipmentDTO) throws EquipmentNotFoundException;

    EquipmentDTO deleteEquipment(Long id) throws EquipmentNotFoundException;

}

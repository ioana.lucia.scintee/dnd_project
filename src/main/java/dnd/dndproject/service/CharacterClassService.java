package dnd.dndproject.service;

import dnd.dndproject.dto.CharacterClassDTO;
import dnd.dndproject.exception.CharacterClassNotFoundException;

import java.util.List;

public interface CharacterClassService {

    List<CharacterClassDTO> getAllClasses();

    CharacterClassDTO getClassById(Long id) throws CharacterClassNotFoundException;

    CharacterClassDTO saveClass(CharacterClassDTO characterClassDTO) throws CharacterClassNotFoundException;

    CharacterClassDTO deleteClass(Long id) throws CharacterClassNotFoundException;

}

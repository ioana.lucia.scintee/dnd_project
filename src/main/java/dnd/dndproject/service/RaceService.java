package dnd.dndproject.service;

import dnd.dndproject.dto.RaceDTO;
import dnd.dndproject.exception.RaceNotFoundException;

import java.util.List;

public interface RaceService {

    List<RaceDTO> getAllRaces();

    RaceDTO getRaceById(Long id) throws RaceNotFoundException;

    RaceDTO saveRace(RaceDTO raceDTO) throws RaceNotFoundException;

    RaceDTO deleteRace(Long id) throws RaceNotFoundException;

}

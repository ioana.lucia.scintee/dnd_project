package dnd.dndproject.service;

import dnd.dndproject.dto.StatsDTO;
import dnd.dndproject.exception.StatsNotFoundException;

import java.util.List;

public interface StatsService {
    List<StatsDTO> getAllStats();

    StatsDTO getStatsById(Long id) throws StatsNotFoundException;

    StatsDTO saveStats(StatsDTO statsDTO) throws StatsNotFoundException;

    StatsDTO deleteStats(Long id) throws StatsNotFoundException;

}

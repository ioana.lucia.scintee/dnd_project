package dnd.dndproject.service.impl;

import dnd.dndproject.dto.CharacterClassDTO;
import dnd.dndproject.entity.CharacterClass;
import dnd.dndproject.exception.CharacterClassNotFoundException;
import dnd.dndproject.repository.CharacterClassRepository;
import dnd.dndproject.service.CharacterClassService;
import dnd.dndproject.service.mapper.CharacterClassMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CharacterClassServiceImpl implements CharacterClassService {

    @Autowired
    private CharacterClassMapper characterClassMapper;
    @Autowired
    private CharacterClassRepository characterClassRepository;

    public List<CharacterClassDTO> getAllClasses() {
        List<CharacterClass> classList = characterClassRepository.findAll();
        return classList
                .stream()
                .map(characterClass -> characterClassMapper.classToDTO(characterClass))
                .collect(Collectors.toList());
    }

    public CharacterClassDTO getClassById(Long id) throws CharacterClassNotFoundException {
        CharacterClass characterClass = characterClassRepository.findById(id).orElse(null);
        CharacterClassDTO result = null;
        if (characterClass != null) {
            result = characterClassMapper.classToDTO(characterClass);
        } else {
            throw new CharacterClassNotFoundException();
        }
        return result;
    }

    public CharacterClassDTO saveClass(CharacterClassDTO characterClassDTO) throws CharacterClassNotFoundException {
        if (characterClassDTO.getId() != null && characterClassDTO.getId() > 0
                && !characterClassRepository.existsById(characterClassDTO.getId())) {
            throw new CharacterClassNotFoundException();
        }
        CharacterClass characterClass = characterClassMapper.classToEntity(characterClassDTO);
        characterClass = characterClassRepository.save(characterClass);
        return characterClassMapper.classToDTO(characterClass);
    }

    public CharacterClassDTO deleteClass(Long id) throws CharacterClassNotFoundException {
        CharacterClassDTO characterClassDTO = getClassById(id);
        characterClassRepository.delete(characterClassMapper.classToEntity(characterClassDTO));
        return characterClassDTO;
    }
}

package dnd.dndproject.service.impl;

import dnd.dndproject.dto.RaceDTO;
import dnd.dndproject.entity.Race;
import dnd.dndproject.exception.RaceNotFoundException;
import dnd.dndproject.repository.RaceRepository;
import dnd.dndproject.service.RaceService;
import dnd.dndproject.service.mapper.RaceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RaceServiceImpl implements RaceService {
    @Autowired
    private RaceMapper raceMapper;
    @Autowired
    private RaceRepository raceRepository;

    public List<RaceDTO> getAllRaces() {
        List<Race> raceList = raceRepository.findAll();
        return raceList
                .stream()
                .map(race -> raceMapper.raceToDTO(race))
                .collect(Collectors.toList());
    }

    public RaceDTO getRaceById(Long id) throws RaceNotFoundException {
        Race race = raceRepository.findById(id).orElse(null);
        RaceDTO result = null;
        if (race != null) {
            result = raceMapper.raceToDTO(race);
        } else {
            throw new RaceNotFoundException();
        }
        return result;
    }

    public RaceDTO saveRace(RaceDTO raceDTO) throws RaceNotFoundException {
        if (raceDTO.getId() != null && raceDTO.getId() > 0
                && !raceRepository.existsById(raceDTO.getId())) {
            throw new RaceNotFoundException();
        }
        Race race = raceMapper.raceToEntity(raceDTO);
        race = raceRepository.save(race);
        return raceMapper.raceToDTO(race);

    }

    public RaceDTO deleteRace(Long id) throws RaceNotFoundException {
        RaceDTO raceDTO = getRaceById(id);
        raceRepository.delete(raceMapper.raceToEntity(raceDTO));
        return raceDTO;
    }
}

package dnd.dndproject.service.impl;

import dnd.dndproject.dto.CampaignDTO;
import dnd.dndproject.entity.Campaign;
import dnd.dndproject.exception.CampaignNotFoundException;
import dnd.dndproject.repository.CampaignRepository;
import dnd.dndproject.service.CampaignService;
import dnd.dndproject.service.mapper.CampaignMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.stream.Collectors;


@Component
public class CampaignServiceImpl implements CampaignService {
    @Autowired
    private CampaignMapper mapper;
    @Autowired
    private CampaignRepository campaignRepository;

    public List<CampaignDTO> getAllCampaigns() {
        List<Campaign> campaignList = campaignRepository.findAll();
        return campaignList
                .stream()
                .map(campaign -> mapper.campaignToDTO(campaign))
                .collect(Collectors.toList());
    }


    public CampaignDTO getCampaignById(Long id) throws CampaignNotFoundException {
        Campaign campaign = campaignRepository.findById(id).orElse(null);
        CampaignDTO result = null;
        if (campaign != null) {
            result = mapper.campaignToDTO(campaign);
        } else {
            throw new CampaignNotFoundException();
        }
        return result;
    }

    public CampaignDTO saveCampaign(CampaignDTO campaignDTO) throws CampaignNotFoundException {
        if (campaignDTO.getId() != null && campaignDTO.getId() > 0
                && !campaignRepository.existsById(campaignDTO.getId())) {
            throw new CampaignNotFoundException();
        }
        Campaign campaign = mapper.campaignToEntity(campaignDTO);
        campaign = campaignRepository.save(campaign);
        return mapper.campaignToDTO(campaign);
    }

    public CampaignDTO deleteCampaign(Long id) throws CampaignNotFoundException {
        CampaignDTO campaignDTO = getCampaignById(id);
        campaignRepository.delete(mapper.campaignToEntity(campaignDTO));
        return campaignDTO;
    }
}

package dnd.dndproject.service.impl;

import dnd.dndproject.dto.CharacterDTO;
import dnd.dndproject.entity.Character;
import dnd.dndproject.exception.CharacterNotFoundException;
import dnd.dndproject.repository.CharacterRepository;
import dnd.dndproject.service.CharacterService;
import dnd.dndproject.service.mapper.CharacterMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


@Component
public class CharacterServiceImpl implements CharacterService {
    @Autowired
    private CharacterMapper characterMapper;

    @Autowired
    private CharacterRepository characterRepository;


    public List<CharacterDTO> getAllCharacters() {
        List<Character> characterList = characterRepository.findAll();
        return characterList
                .stream()
                .map(character -> characterMapper.characterToDTO(character))
                .collect(Collectors.toList());
    }


    public CharacterDTO getCharacterById(Long id) throws CharacterNotFoundException {
        Character character = characterRepository.findById(id).orElse(null);
        CharacterDTO result = null;
        if (character != null) {
            result = characterMapper.characterToDTO(character);
        } else {
            throw new CharacterNotFoundException();
        }
        return result;
    }

    public CharacterDTO saveCharacter(CharacterDTO characterDTO) throws CharacterNotFoundException {
        if (characterDTO.getId() != null && characterDTO.getId() > 0
                && !characterRepository.existsById(characterDTO.getId())) {
            throw new CharacterNotFoundException();
        }
        Character character = characterMapper.characterToEntity(characterDTO);
        character = characterRepository.save(character);
        return characterMapper.characterToDTO(character);
    }

    public CharacterDTO deleteCharacter(Long id) throws CharacterNotFoundException {
        CharacterDTO characterDTO = getCharacterById(id);
        characterRepository.delete(characterMapper.characterToEntity(characterDTO));
        return characterDTO;
    }
}

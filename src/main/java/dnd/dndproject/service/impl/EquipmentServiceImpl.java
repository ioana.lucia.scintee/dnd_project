package dnd.dndproject.service.impl;

import dnd.dndproject.dto.EquipmentDTO;
import dnd.dndproject.entity.Equipment;
import dnd.dndproject.exception.EquipmentNotFoundException;
import dnd.dndproject.repository.EquipmentRepository;
import dnd.dndproject.service.EquipmentService;
import dnd.dndproject.service.mapper.EquipmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EquipmentServiceImpl implements EquipmentService {

    @Autowired
    private EquipmentMapper equipmentMapper;
    @Autowired
    private EquipmentRepository equipmentRepository;

    public List<EquipmentDTO> getAllEquipment() {
        List<Equipment> equipmentList = equipmentRepository.findAll();
        return equipmentList
                .stream()
                .map(equipment -> equipmentMapper.equipmentToDTO(equipment))
                .collect(Collectors.toList());
    }

    public EquipmentDTO getEquipmentById(Long id) throws EquipmentNotFoundException {
        Equipment equipment = equipmentRepository.findById(id).orElse(null);
        EquipmentDTO result = null;
        if (equipment != null) {
            result = equipmentMapper.equipmentToDTO(equipment);
        } else {
            throw new EquipmentNotFoundException();
        }
        return result;
    }

    public EquipmentDTO saveEquipment(EquipmentDTO equipmentDTO) throws EquipmentNotFoundException {
        if (equipmentDTO.getId() != null && equipmentDTO.getId() > 0
                && !equipmentRepository.existsById(equipmentDTO.getId())) {
            throw new EquipmentNotFoundException();
        }
        Equipment equipment = equipmentMapper.equipmentToEntity(equipmentDTO);
        equipment = equipmentRepository.save(equipment);
        return equipmentMapper.equipmentToDTO(equipment);

    }

    public EquipmentDTO deleteEquipment(Long id) throws EquipmentNotFoundException {
        EquipmentDTO equipmentDTO = getEquipmentById(id);
        equipmentRepository.delete(equipmentMapper.equipmentToEntity(equipmentDTO));
        return equipmentDTO;
    }
}

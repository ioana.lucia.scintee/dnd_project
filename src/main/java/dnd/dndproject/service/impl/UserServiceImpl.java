package dnd.dndproject.service.impl;

import dnd.dndproject.dto.AbilityDTO;
import dnd.dndproject.dto.UserCreateDTO;
import dnd.dndproject.dto.UserDTO;
import dnd.dndproject.entity.Ability;
import dnd.dndproject.entity.AppUser;
import dnd.dndproject.exception.AbilityNotFoundException;
import dnd.dndproject.exception.UserNotFoundException;
import dnd.dndproject.repository.UserRepository;
import dnd.dndproject.service.UserService;
import dnd.dndproject.service.mapper.UserMapper;
import org.springframework.stereotype.Component;


@Component
public class UserServiceImpl implements UserService {

    private  UserMapper userMapper ;
    private  UserRepository userRepository ;


    public UserServiceImpl(UserMapper userMapper, UserRepository userRepository) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
    }

    @Override
    public UserDTO create(UserCreateDTO createDTO) {
        AppUser toBeSaved = userMapper.userToEntity(createDTO);
        AppUser created = userRepository.save(toBeSaved);
        return userMapper.userToDTO(created);
    }


    @Override
    public UserDTO saveUser(UserDTO userDTO) throws UserNotFoundException {
        if (userDTO.getId() != null && userDTO.getId() > 0
                && !userRepository.existsById(userDTO.getId())) {
            throw new UserNotFoundException();
        }
        AppUser appUser = userMapper.userToEntity(new UserCreateDTO());
        appUser = userRepository.save(appUser);
        return userMapper.userToDTO(appUser);

    }

    @Override
    public UserDTO getUsersById(Long id) throws UserNotFoundException {
        AppUser users = userRepository.findById(id).orElse(null);
        UserDTO result = null;
        if (users != null) {
            result = userMapper.userToDTO(users);
        } else {
            throw new UserNotFoundException();
        }
        return result;
    }

    @Override
    public UserDTO getUsersByUsername() {
        return null;
    }

}

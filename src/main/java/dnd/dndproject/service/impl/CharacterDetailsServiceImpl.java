package dnd.dndproject.service.impl;

import dnd.dndproject.dto.CharacterDetailsDTO;
import dnd.dndproject.entity.CharacterDetails;
import dnd.dndproject.exception.CharacterDetailsNotFoundException;
import dnd.dndproject.repository.CharacterDetailsRepository;
import dnd.dndproject.service.CharacterDetailsService;
import dnd.dndproject.service.mapper.CharacterDetailsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CharacterDetailsServiceImpl implements CharacterDetailsService {
    @Autowired
    private CharacterDetailsMapper characterDetailsMapper;
    @Autowired
    private CharacterDetailsRepository characterDetailsRepository;


    public List<CharacterDetailsDTO> getAllDetails() {
        List<CharacterDetails> detailList = characterDetailsRepository.findAll();
        return detailList
                .stream()
                .map(characterDetails -> characterDetailsMapper.detailsToDTO(characterDetails))
                .collect(Collectors.toList());
    }

    public CharacterDetailsDTO getDetailById(Long id) throws CharacterDetailsNotFoundException {
        CharacterDetails characterDetails = characterDetailsRepository.findById(id).orElse(null);
        CharacterDetailsDTO result = null;
        if (characterDetails != null) {
            result = characterDetailsMapper.detailsToDTO(characterDetails);
        } else {
            throw new CharacterDetailsNotFoundException();
        }
        return result;
    }

    public CharacterDetailsDTO saveDetail(CharacterDetailsDTO characterDetailsDTO) throws CharacterDetailsNotFoundException {
        if (characterDetailsDTO.getId() != null && characterDetailsDTO.getId() > 0
                && !characterDetailsRepository.existsById(characterDetailsDTO.getId())) {
            throw new CharacterDetailsNotFoundException();
        }
        CharacterDetails characterDetails = characterDetailsMapper.detailsToEntity(characterDetailsDTO);
        characterDetails = characterDetailsRepository.save(characterDetails);
        return characterDetailsMapper.detailsToDTO(characterDetails);
    }

    public CharacterDetailsDTO deleteDetail(Long id) throws CharacterDetailsNotFoundException {
        CharacterDetailsDTO characterDetailsDTO = getDetailById(id);
        characterDetailsRepository.delete(characterDetailsMapper.detailsToEntity(characterDetailsDTO));
        return characterDetailsDTO;
    }
}

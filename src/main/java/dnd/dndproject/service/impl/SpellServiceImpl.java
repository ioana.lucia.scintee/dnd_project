package dnd.dndproject.service.impl;

import dnd.dndproject.dto.SpellDTO;
import dnd.dndproject.entity.Spell;
import dnd.dndproject.exception.SpellNotFoundException;
import dnd.dndproject.repository.SpellRepository;
import dnd.dndproject.service.SpellService;
import dnd.dndproject.service.mapper.SpellMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SpellServiceImpl implements SpellService {

    @Autowired
    private SpellMapper spellMapper;
    @Autowired
    private SpellRepository spellRepository;

    public List<SpellDTO> getAllSpells() {
        List<Spell> spellList = spellRepository.findAll();
        return spellList
                .stream()
                .map(spell -> spellMapper.spellToDTO(spell))
                .collect(Collectors.toList());
    }

    public SpellDTO getSpellById(Long id) throws SpellNotFoundException {
        Spell spell = spellRepository.findById(id).orElse(null);
        SpellDTO result = null;
        if (spell != null) {
            result = spellMapper.spellToDTO(spell);
        } else {
            throw new SpellNotFoundException();
        }
        return result;
    }

    public SpellDTO saveSpell(SpellDTO spellDTO) throws SpellNotFoundException {
        if (spellDTO.getId() != null && spellDTO.getId() > 0
                && !spellRepository.existsById(spellDTO.getId())) {
            throw new SpellNotFoundException();
        }
        Spell spell = spellMapper.spellToEntity(spellDTO);
        spell = spellRepository.save(spell);
        return spellMapper.spellToDTO(spell);

    }

    public SpellDTO deleteSpell(Long id) throws SpellNotFoundException {
        SpellDTO spellDTO = getSpellById(id);
        spellRepository.delete(spellMapper.spellToEntity(spellDTO));
        return spellDTO;
    }
}

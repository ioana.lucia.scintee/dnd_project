package dnd.dndproject.service.impl;

import dnd.dndproject.dto.PetDTO;
import dnd.dndproject.entity.Pet;
import dnd.dndproject.exception.PetNotFoundException;
import dnd.dndproject.repository.PetRepository;
import dnd.dndproject.service.PetService;
import dnd.dndproject.service.mapper.PetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PetServiceImpl implements PetService {


    @Autowired
    private PetMapper petMapper;
    @Autowired
    private PetRepository petRepository;

    public List<PetDTO> getAllPets() {
        List<Pet> petList = petRepository.findAll();
        return petList
                .stream()
                .map(pet -> petMapper.petToDTO(pet))
                .collect(Collectors.toList());
    }

    public PetDTO getPetById(Long id) throws PetNotFoundException {
        Pet pet = petRepository.findById(id).orElse(null);
        PetDTO result = null;
        if (pet != null) {
            result = petMapper.petToDTO(pet);
        } else {
            throw new PetNotFoundException();
        }
        return result;
    }

    public PetDTO savePet(PetDTO petDTO) throws PetNotFoundException {
        if (petDTO.getId() != null && petDTO.getId() > 0
                && !petRepository.existsById(petDTO.getId())) {
            throw new PetNotFoundException();
        }
        Pet pet = petMapper.petToEntity(petDTO);
        pet = petRepository.save(pet);
        return petMapper.petToDTO(pet);

    }

    public PetDTO deletePet(Long id) throws PetNotFoundException {
        PetDTO petDTO = getPetById(id);
        petRepository.delete(petMapper.petToEntity(petDTO));
        return petDTO;
    }
}

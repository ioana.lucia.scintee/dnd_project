package dnd.dndproject.service.impl;

import dnd.dndproject.dto.AbilityDTO;
import dnd.dndproject.entity.Ability;
import dnd.dndproject.exception.AbilityNotFoundException;
import dnd.dndproject.repository.AbilityRepository;
import dnd.dndproject.service.AbilityService;
import dnd.dndproject.service.mapper.AbilityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AbilityServiceImpl implements AbilityService {

    @Autowired
    private AbilityMapper abilityMapper;
    @Autowired
    private AbilityRepository abilityRepository;

    public List<AbilityDTO> getAllAbilities() {
        List<Ability> abilityList = abilityRepository.findAll();
        return abilityList
                .stream()
                .map(ability -> abilityMapper.abilityToDTO(ability))
                .collect(Collectors.toList());
    }

    public AbilityDTO getAbilityById(Long id) throws AbilityNotFoundException {
        Ability ability = abilityRepository.findById(id).orElse(null);
        AbilityDTO result = null;
        if (ability != null) {
            result = abilityMapper.abilityToDTO(ability);
        } else {
            throw new AbilityNotFoundException();
        }
        return result;
    }

    public AbilityDTO saveAbility(AbilityDTO abilityDTO) throws AbilityNotFoundException {
        if (abilityDTO.getId() != null && abilityDTO.getId() > 0
                && !abilityRepository.existsById(abilityDTO.getId())) {
            throw new AbilityNotFoundException();
        }
        Ability ability = abilityMapper.abilityToEntity(abilityDTO);
        ability = abilityRepository.save(ability);
        return abilityMapper.abilityToDTO(ability);
    }

    public AbilityDTO deleteAbility(Long id) throws AbilityNotFoundException {
        AbilityDTO abilityDTO = getAbilityById(id);
        abilityRepository.delete(abilityMapper.abilityToEntity(abilityDTO));
        return abilityDTO;
    }
}

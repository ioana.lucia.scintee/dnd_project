package dnd.dndproject.service.impl;


import dnd.dndproject.dto.StatsDTO;
import dnd.dndproject.entity.Stats;
import dnd.dndproject.exception.StatsNotFoundException;
import dnd.dndproject.repository.StatsRepository;
import dnd.dndproject.service.StatsService;
import dnd.dndproject.service.mapper.StatsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class StatsServiceImpl implements StatsService {
    @Autowired
    private StatsMapper statsMapper;
    @Autowired
    private StatsRepository statsRepository;

    public List<StatsDTO> getAllStats() {
        List<Stats> statsList = statsRepository.findAll();
        return statsList
                .stream()
                .map(stats -> statsMapper.statsToDTO(stats))
                .collect(Collectors.toList());
    }

    public StatsDTO getStatsById(Long id) throws StatsNotFoundException {
        Stats stats = statsRepository.findById(id).orElse(null);
        StatsDTO result = null;
        if (stats != null) {
            statsMapper.statsToDTO(stats);
        } else {
            throw new StatsNotFoundException();
        }
        return result;
    }

    public StatsDTO saveStats(StatsDTO statsDTO) throws StatsNotFoundException {
        if (statsDTO.getId() != null && statsDTO.getId() > 0
                && !statsRepository.existsById(statsDTO.getId())) {
            throw new StatsNotFoundException();
        }
        Stats stats = statsMapper.statsToEntity(statsDTO);
        stats = statsRepository.save(stats);
        return statsMapper.statsToDTO(stats);

    }

    public StatsDTO deleteStats(Long id) throws StatsNotFoundException {
        StatsDTO statsDTO = getStatsById(id);
        statsRepository.delete(statsMapper.statsToEntity(statsDTO));
        return statsDTO;
    }
}

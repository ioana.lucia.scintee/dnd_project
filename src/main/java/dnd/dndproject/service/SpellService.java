package dnd.dndproject.service;

import dnd.dndproject.dto.SpellDTO;
import dnd.dndproject.exception.SpellNotFoundException;

import java.util.List;

public interface SpellService {
    List<SpellDTO> getAllSpells();

    SpellDTO getSpellById(Long id) throws SpellNotFoundException;

    SpellDTO saveSpell(SpellDTO spellDTO) throws SpellNotFoundException;

    SpellDTO deleteSpell(Long id) throws SpellNotFoundException;

}

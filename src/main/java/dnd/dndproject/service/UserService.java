package dnd.dndproject.service;

import dnd.dndproject.dto.UserCreateDTO;
import dnd.dndproject.dto.UserDTO;
import dnd.dndproject.exception.UserNotFoundException;


public interface UserService {

    UserDTO create(UserCreateDTO createDTO);

    UserDTO saveUser(UserDTO userDTO) throws UserNotFoundException;
    UserDTO getUsersById(Long id) throws UserNotFoundException;

    UserDTO getUsersByUsername();
}

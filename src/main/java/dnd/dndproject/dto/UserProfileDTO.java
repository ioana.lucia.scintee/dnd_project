package dnd.dndproject.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class UserProfileDTO {

    private Long id;

    private String userName;

    private String password;

    private String email;


}
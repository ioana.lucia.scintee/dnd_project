package dnd.dndproject.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StrengthDTO {
    private Long id;
    private String name;
    private Integer totalScore;
    private double modifier;
    private Integer baseScore;
    private Integer racialBonus;
}

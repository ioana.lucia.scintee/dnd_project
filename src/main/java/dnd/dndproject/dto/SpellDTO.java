package dnd.dndproject.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class SpellDTO {

    private Long id;

    private String name;

    private Integer level;

    private Double castingTime;

    private Double range;

    private Integer damagePoints;

    private boolean buff;

    private boolean debuff;

    private Double buffDuration;

    private Double debuffDuration;

    private String elementType;

    private String description;
}

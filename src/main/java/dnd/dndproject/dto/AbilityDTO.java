package dnd.dndproject.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AbilityDTO {
    private Long id;
}

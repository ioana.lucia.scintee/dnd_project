package dnd.dndproject.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EquipmentDTO {
    private Long id;

    private String name;

    private Integer damagePoints;

    private boolean buff;

    private boolean debuff;

    private Double buffDuration;

    private Double debuffDuration;

}
package dnd.dndproject.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CharacterDTO {
    private Long id;

    private String name;

    private String backstory;
}

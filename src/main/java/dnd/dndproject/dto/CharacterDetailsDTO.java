package dnd.dndproject.dto;

import dnd.dndproject.entity.Alignament;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CharacterDetailsDTO {
    private Long id;

    private Alignament alignament;

    private String faith;
}

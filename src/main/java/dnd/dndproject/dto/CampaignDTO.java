package dnd.dndproject.dto;

import lombok.*;

import java.time.LocalDate;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CampaignDTO {
    private Long id;

    private String name;

    private Integer minLevel;

    private LocalDate beginDate;

    private LocalDate endDate;
}

package dnd.dndproject.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StaminaDTO {

    private Long id;

    private Integer numberOfPoints;
}

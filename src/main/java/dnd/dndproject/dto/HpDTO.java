package dnd.dndproject.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HpDTO {

    private Long id;

    private Integer numberOfPoints;
}

package dnd.dndproject.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CharacterClassDTO {
    private Long id;

    private String name;

    private String primaryAbility;

}

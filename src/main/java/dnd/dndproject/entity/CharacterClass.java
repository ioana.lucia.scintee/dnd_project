package dnd.dndproject.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CharacterClass {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private String primaryAbility;


    @OneToMany(mappedBy = "characterClass")
    private List<Spell> spells;

    @OneToOne(mappedBy = "characterClass")
    private Character character;

}

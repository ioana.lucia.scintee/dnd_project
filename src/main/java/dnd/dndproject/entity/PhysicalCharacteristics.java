package dnd.dndproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "physical_characteristics")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class PhysicalCharacteristics {
    @Id
    private Long id;

    @Enumerated(EnumType.STRING)
    private Hair hair;

    @Enumerated(EnumType.STRING)
    private Eyes eyes;

    private Double height;

    private Double weight;

    private Integer age;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;

    @OneToOne(mappedBy = "characteristics")
    private CharacterDetails characterDetails;
}

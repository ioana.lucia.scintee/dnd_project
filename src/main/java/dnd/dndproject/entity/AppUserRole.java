package dnd.dndproject.entity;

public enum AppUserRole {
    ADMIN,
    USER
}

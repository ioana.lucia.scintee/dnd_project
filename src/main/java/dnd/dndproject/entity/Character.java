package dnd.dndproject.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "character")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Character {
    @Id
    @GeneratedValue(strategy = SEQUENCE)
    private Long id;
    private String name;
    private String backstory;


    @OneToOne
    @JoinColumn(name = "race_id")
    private Race race;

    @OneToOne
    @JoinColumn(name = "character_class_id")
    private CharacterClass characterClass;

    @OneToOne
    @JoinColumn(name = "stats_id")
    private Stats stats;

    @OneToOne
    @JoinColumn(name = "ability_id")
    private Ability ability;

    @ManyToOne
    @JoinColumn(name = "app_user_id",
                foreignKey=@ForeignKey(name = "character_app_user_id_fkey")
                )
    private AppUser appUser;

    @OneToMany(mappedBy = "character")
    private List<Pet> pets;

    @OneToMany(mappedBy = "character")
    private List<Equipment> equipments;

    @OneToMany(mappedBy = "character")
    private List<Campaign> campaigns;
    @ManyToMany()
    @JoinTable(
            name = "character_to_campaigns",
            joinColumns = {@JoinColumn(name = "campaign_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    private Set<Campaign> campaignList;

    @ManyToMany(mappedBy = "characters")
    private Set<Spell> spells;

    @OneToOne
    @JoinColumn(name = "character_detail_id")
    private CharacterDetails characterDetails;
}

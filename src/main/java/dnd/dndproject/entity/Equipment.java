package dnd.dndproject.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Equipment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private Integer damagePoints;

    private boolean buff;

    private boolean debuff;

    private Double buffDuration;

    private Double debuffDuration;

    @ManyToOne
    @JoinColumn(name = "character_id")
    private Character character;
}

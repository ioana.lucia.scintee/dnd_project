package dnd.dndproject.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)

    private Long id;

    private String userName;

    private String password;

    private String email;

    private Integer ownedCharacter;

    @Enumerated(value = EnumType.STRING)
    private AppUserRole role;



    @OneToMany(mappedBy = "appUser")
    private List<Character> characters;

    @ManyToMany()
    @JoinTable(
            name = "app_user_to_campaigns",
            joinColumns = {@JoinColumn(name = "app_user_id")},
            inverseJoinColumns = {@JoinColumn(name = "campaign_id")}
    )
    private Set<Campaign> campaignList;
}
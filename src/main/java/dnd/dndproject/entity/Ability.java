package dnd.dndproject.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Ability {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @OneToOne
    @JoinColumn(name = "intelligence_id")
    private Intelligence intelligence;

    @OneToOne
    @JoinColumn(name = "dexterity_id")
    private Dexterity dexterity;

    @OneToOne
    @JoinColumn(name = "constitution_id")
    private Constitution constitution;

    @OneToOne
    @JoinColumn(name = "wisdom_id")
    private Wisdom wisdom;

    @OneToOne
    @JoinColumn(name = "charisma_id")
    private Charisma charisma;

    @OneToOne
    @JoinColumn(name = "strength_id")
    private Strength strength;

    @OneToOne(mappedBy = "ability")
    private Character character;

}

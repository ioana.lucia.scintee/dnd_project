package dnd.dndproject.entity;
import lombok.*;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Stamina {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    private Integer numberOfPoints;

    @OneToOne(mappedBy = "stamina")
    private Stats stats;
}

package dnd.dndproject.entity;

import lombok.*;

import javax.persistence.*;
import java.awt.*;
import java.util.Set;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class Spell {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private Integer level;

    private Double castingTime;

    private Double range;

    private Integer damagePoints;

    private boolean buff;

    private boolean debuff;

    private Double buffDuration;

    private Double debuffDuration;

    private String elementType;

    private String description;

    @ManyToOne
    @JoinColumn(name = "character_class_id")
    private CharacterClass characterClass;
    @ManyToMany()
    @JoinTable(
            name = "spells_to_characters",
            joinColumns = {@JoinColumn(name = "spell_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    private Set<Character> characters;
}

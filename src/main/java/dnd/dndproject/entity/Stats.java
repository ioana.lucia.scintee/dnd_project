package dnd.dndproject.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Stats {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @OneToOne
    @JoinColumn(name = "hp_id")
    private Hp characterHp;

    @OneToOne
    @JoinColumn(name = "mana_id")
    private Mana mana;

    @OneToOne
    @JoinColumn(name = "stamina_id")
    private Stamina stamina;

    @OneToOne(mappedBy = "stats")
    private Character character;

}

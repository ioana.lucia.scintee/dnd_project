package dnd.dndproject.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "character_detail")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class CharacterDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private Alignament alignament;
    private String faith;

    @OneToOne
    @JoinColumn(name = "physical_characteristics_id")
    private PhysicalCharacteristics characteristics;

    @OneToOne(mappedBy = "characterDetails")
    private Character character;


}

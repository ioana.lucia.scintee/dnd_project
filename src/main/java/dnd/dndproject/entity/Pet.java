package dnd.dndproject.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;

    @OneToOne
    @JoinColumn(name = "hp_id")
    private Hp petHp;

    @ManyToOne
    @JoinColumn(name = "character_id")
    private Character character;
}

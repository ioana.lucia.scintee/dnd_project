package dnd.dndproject.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Campaign {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private Integer minLevel;

    private LocalDate beginDate;

    private LocalDate endDate;

    @ManyToOne
    @JoinColumn(name = "campaign_id")
    private Character character;

    @ManyToMany(mappedBy = "campaignList")
    private Set<Character> characters;

    @ManyToMany(mappedBy = "campaignList")
    private Set<AppUser> appUsers;
}

package dnd.dndproject.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Hp {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private Integer numberOfPoints;

    @OneToOne(mappedBy = "petHp")
    private Pet pet;

    @OneToOne(mappedBy = "characterHp")
    private Stats stats;


}

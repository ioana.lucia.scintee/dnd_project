package dnd.dndproject.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Wisdom {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private Integer totalScore;

    private double modifier;

    private Integer baseScore;

    private String racialBonus;

    @OneToOne(mappedBy = "wisdom")
    private Ability ability;
}

package dnd.dndproject.repository;

import dnd.dndproject.entity.Stamina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StaminaRepository extends JpaRepository<Stamina, Long> {
}

package dnd.dndproject.repository;

import dnd.dndproject.entity.Intelligence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IntelligenceRepository extends JpaRepository<Intelligence, Long> {
}

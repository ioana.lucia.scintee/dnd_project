package dnd.dndproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import dnd.dndproject.entity.Stats;
import org.springframework.stereotype.Repository;

@Repository
public interface StatsRepository extends JpaRepository<Stats, Long> {
}

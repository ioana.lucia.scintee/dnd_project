package dnd.dndproject.repository;

import dnd.dndproject.entity.Mana;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManaRepository extends JpaRepository<Mana, Long> {
}

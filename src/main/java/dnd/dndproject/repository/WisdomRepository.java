package dnd.dndproject.repository;

import dnd.dndproject.entity.Wisdom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WisdomRepository extends JpaRepository<Wisdom, Long> {
}

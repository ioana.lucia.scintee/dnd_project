package dnd.dndproject.repository;

import dnd.dndproject.entity.Hp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HpRepository extends JpaRepository<Hp, Long> {
}

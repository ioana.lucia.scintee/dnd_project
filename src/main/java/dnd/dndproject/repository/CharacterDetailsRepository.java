package dnd.dndproject.repository;

import dnd.dndproject.entity.CharacterDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterDetailsRepository extends JpaRepository<CharacterDetails, Long> {
}

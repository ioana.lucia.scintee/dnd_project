package dnd.dndproject.repository;

import dnd.dndproject.entity.Charisma;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharismaRepository extends JpaRepository<Charisma, Long> {
}

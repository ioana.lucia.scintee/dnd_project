package dnd.dndproject.exception;

public class RaceNotFoundException extends Exception {
    private static final String MESSAGE = "The race specified is not found";

    public RaceNotFoundException() {
        super(MESSAGE);
    }
}

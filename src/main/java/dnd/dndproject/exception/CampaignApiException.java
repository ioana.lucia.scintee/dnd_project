package dnd.dndproject.exception;

public class CampaignApiException extends Exception {
    private static final String MESSAGE = "Generic campaign api not found";

    public CampaignApiException() {
        super(MESSAGE);
    }

    public CampaignApiException(String message) {
        super(message);
    }
}

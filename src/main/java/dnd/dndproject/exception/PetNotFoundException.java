package dnd.dndproject.exception;

public class PetNotFoundException extends Exception {
    private static final String MESSAGE = "The pet specified is not found";

    public PetNotFoundException() {
        super(MESSAGE);
    }
}

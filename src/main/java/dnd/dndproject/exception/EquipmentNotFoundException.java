package dnd.dndproject.exception;

public class EquipmentNotFoundException extends Exception {
    private static final String MESSAGE = "The equipment specified is not found";

    public EquipmentNotFoundException() {
        super(MESSAGE);
    }
}

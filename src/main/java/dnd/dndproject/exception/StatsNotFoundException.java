package dnd.dndproject.exception;

public class StatsNotFoundException extends Exception {

    private static final String MESSAGE = "The stats specified are not found";

    public StatsNotFoundException() {
        super(MESSAGE);
    }
}

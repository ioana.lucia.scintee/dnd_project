package dnd.dndproject.exception;

public class CharacterClassNotFoundException extends Exception {
    private static final String MESSAGE = "The class specified is not found";

    public CharacterClassNotFoundException() {
        super(MESSAGE);
    }
}

package dnd.dndproject.exception;

public class UserNotFoundException extends CampaignApiException {

    private static final String MESSAGE = "The user specified is not found! ";

    public UserNotFoundException() {
        super(MESSAGE);
    }

}

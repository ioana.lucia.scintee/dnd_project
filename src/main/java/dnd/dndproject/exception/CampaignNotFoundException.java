package dnd.dndproject.exception;

public class CampaignNotFoundException extends CampaignApiException {

    private static final String MESSAGE = "The campaign specified is not found";

    public CampaignNotFoundException() {
        super(MESSAGE);
    }

}


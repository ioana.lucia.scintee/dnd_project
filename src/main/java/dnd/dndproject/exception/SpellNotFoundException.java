package dnd.dndproject.exception;

public class SpellNotFoundException extends Exception {
    private static final String MESSAGE = "The spell specified is not found";

    public SpellNotFoundException() {
        super(MESSAGE);
    }
}

package dnd.dndproject.exception;


public class AbilityNotFoundException extends Exception {
    private static final String MESSAGE = "The ability specified is not found";

    public AbilityNotFoundException() {
        super(MESSAGE);
    }
}


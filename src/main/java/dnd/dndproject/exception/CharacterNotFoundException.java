package dnd.dndproject.exception;

public class CharacterNotFoundException extends Exception {
    private static final String MESSAGE = "The character specified is not found";

    public CharacterNotFoundException() {
        super(MESSAGE);
    }

}

package dnd.dndproject.exception;

public class CharacterDetailsNotFoundException extends Exception {
    private static final String MESSAGE = "The details specified are not found";

    public CharacterDetailsNotFoundException() {
        super(MESSAGE);
    }
}

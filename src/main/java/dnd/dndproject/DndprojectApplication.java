package dnd.dndproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DndprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(DndprojectApplication.class, args);
	}

}

package dnd.dndproject.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class DndPasswordConfig {

    @Bean
    PasswordEncoder getPassEncoder() {
        return new BCryptPasswordEncoder(9);
    }
}

package dnd.dndproject.security;

import dnd.dndproject.entity.AppUser;
import dnd.dndproject.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class DndUserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    PasswordEncoder encoder;
    UserRepository userRepository;

    public DndUserDetailsService(PasswordEncoder encoder, UserRepository userRepository) {
        this.encoder = encoder;
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser appUser = userRepository.findByUserName(username);
        DndUserDetails userDetails = new DndUserDetails(appUser);

        return userDetails;
    }

}

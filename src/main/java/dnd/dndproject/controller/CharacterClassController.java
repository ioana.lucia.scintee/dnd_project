package dnd.dndproject.controller;

import dnd.dndproject.dto.CharacterClassDTO;
import dnd.dndproject.exception.CharacterClassNotFoundException;
import dnd.dndproject.service.impl.CharacterClassServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CharacterClassController {
    @Autowired
    private CharacterClassServiceImpl characterClassServiceImpl;

    ResponseEntity responseEntity;


    @GetMapping("/classes")
    public ResponseEntity<List<CharacterClassDTO>> showClasses() {
        return new ResponseEntity<>(characterClassServiceImpl
                .getAllClasses(),
                HttpStatus.OK);
    }

    @GetMapping("/classes/{id}")
    public ResponseEntity<CharacterClassDTO> getClassById(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(characterClassServiceImpl.getClassById(id), HttpStatus.OK);
        } catch (CharacterClassNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }


    @PostMapping("/classes")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CharacterClassDTO> addClass(@RequestBody CharacterClassDTO characterClassDTO) {
        try {
            responseEntity = new ResponseEntity(characterClassServiceImpl.saveClass(characterClassDTO), HttpStatus.OK);
        } catch (CharacterClassNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PutMapping("/classes")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CharacterClassDTO> updateClass(@RequestBody CharacterClassDTO characterClassDTO) {
        try {
            responseEntity = new ResponseEntity(characterClassServiceImpl.saveClass(characterClassDTO), HttpStatus.OK);
        } catch (CharacterClassNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @DeleteMapping("/classes/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CharacterClassDTO> deleteClass(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(characterClassServiceImpl.deleteClass(id), HttpStatus.OK);
        } catch (CharacterClassNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }
}

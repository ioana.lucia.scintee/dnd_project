package dnd.dndproject.controller;

import dnd.dndproject.dto.AbilityDTO;
import dnd.dndproject.exception.AbilityNotFoundException;
import dnd.dndproject.service.impl.AbilityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AbilityController {

    @Autowired
    private AbilityServiceImpl abilityServiceImpl;
    ResponseEntity responseEntity;

    @GetMapping("/abilities")
    public ResponseEntity<List<AbilityDTO>> showCharacters() {
        return new ResponseEntity<>(abilityServiceImpl
                .getAllAbilities(),
                HttpStatus.OK);
    }

    @GetMapping("/abilities/{id}")
    public ResponseEntity<AbilityDTO> getAbilityById(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(abilityServiceImpl.getAbilityById(id), HttpStatus.OK);
        } catch (AbilityNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @PostMapping("/abilities")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<AbilityDTO> addAbility(@RequestBody AbilityDTO abilityDTO) {
        try {
            responseEntity = new ResponseEntity(abilityServiceImpl.saveAbility(abilityDTO), HttpStatus.OK);
        } catch (AbilityNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PutMapping("/abilities")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<AbilityDTO> updateAbility(@RequestBody AbilityDTO abilityDTO) {
        try {
            responseEntity = new ResponseEntity(abilityServiceImpl.saveAbility(abilityDTO), HttpStatus.OK);
        } catch (AbilityNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @DeleteMapping("/abilities/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<AbilityDTO> deleteAbility(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(abilityServiceImpl.deleteAbility(id), HttpStatus.OK);
        } catch (AbilityNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }
}

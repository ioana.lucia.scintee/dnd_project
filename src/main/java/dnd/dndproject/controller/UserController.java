package dnd.dndproject.controller;

import dnd.dndproject.dto.StatsDTO;
import dnd.dndproject.dto.UserCreateDTO;
import dnd.dndproject.dto.UserDTO;
import dnd.dndproject.exception.StatsNotFoundException;
import dnd.dndproject.exception.UserNotFoundException;
import dnd.dndproject.security.DndUserDetails;
import dnd.dndproject.service.UserService;
import dnd.dndproject.service.impl.UserServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.security.InvalidParameterException;
import java.util.List;


@RestController
@RequestMapping("api/users")
public class UserController {

    private ResponseEntity responseEntity;


    private final UserService service;
    private UserServiceImpl userServiceImpl;

    public UserController(UserService service) {
        this.service = service;
    }


    @PostMapping("/signup")
    public ResponseEntity<UserDTO> create(@RequestBody UserCreateDTO createDTO) {
        UserDTO created = service.create(createDTO);
        return ResponseEntity.ok(created);
    }
    @GetMapping("users/{id}")
    public ResponseEntity<UserDTO> getUsersById(@PathVariable Long id, UsernamePasswordAuthenticationToken token) throws UserNotFoundException {

        try {
            responseEntity = new ResponseEntity(userServiceImpl.getUsersById(id), HttpStatus.OK);
        } catch (UserNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        DndUserDetails requestingUser = (DndUserDetails) token.getPrincipal();
        if(requestingUser.getUsername().equals(id)){
            return ResponseEntity.ok(service.getUsersById(id));
        } else {
            throw new InvalidParameterException("Requested user is not your user");
        }
    }

    @GetMapping
    public List<UserDTO> getUsers() {
        return List.of(new UserDTO());
    }
}



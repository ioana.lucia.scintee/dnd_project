package dnd.dndproject.controller;

import dnd.dndproject.dto.EquipmentDTO;
import dnd.dndproject.exception.EquipmentNotFoundException;
import dnd.dndproject.service.impl.EquipmentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EquipmentController {
    @Autowired
    private EquipmentServiceImpl equipmentServiceImpl;
    ResponseEntity responseEntity;

    @GetMapping("/equipment")
    public ResponseEntity<List<EquipmentDTO>> showEquipment() {
        return new ResponseEntity<>(equipmentServiceImpl
                .getAllEquipment(),
                HttpStatus.OK);
    }

    @GetMapping("/equipment/{id}")
    public ResponseEntity<EquipmentDTO> getEquipmentById(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(equipmentServiceImpl.getEquipmentById(id), HttpStatus.OK);
        } catch (EquipmentNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @PostMapping("/equipment")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<EquipmentDTO> addEquipment(@RequestBody EquipmentDTO equipmentDTO) {
        try {
            responseEntity = new ResponseEntity(equipmentServiceImpl.saveEquipment(equipmentDTO), HttpStatus.OK);
        } catch (EquipmentNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PutMapping("/equipment")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<EquipmentDTO> updateEquipment(@RequestBody EquipmentDTO equipmentDTO) {
        try {
            responseEntity = new ResponseEntity(equipmentServiceImpl.saveEquipment(equipmentDTO), HttpStatus.OK);
        } catch (EquipmentNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @DeleteMapping("/equipment/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<EquipmentDTO> deleteEquipment(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(equipmentServiceImpl.deleteEquipment(id), HttpStatus.OK);
        } catch (EquipmentNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }
}

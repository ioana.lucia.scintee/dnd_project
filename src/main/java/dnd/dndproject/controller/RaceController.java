package dnd.dndproject.controller;

import dnd.dndproject.dto.RaceDTO;
import dnd.dndproject.exception.RaceNotFoundException;
import dnd.dndproject.service.impl.RaceServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RaceController {

    @Autowired
    private RaceServiceImpl raceServiceImpl;
    private ResponseEntity responseEntity;


    @GetMapping("/races")
    public ResponseEntity<List<RaceDTO>> showRaces() {
        return new ResponseEntity<>(raceServiceImpl.getAllRaces(), HttpStatus.OK);
    }

    @GetMapping("/races/{id}")
    public ResponseEntity<RaceDTO> getRaceById(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(raceServiceImpl.getRaceById(id), HttpStatus.OK);
        } catch (RaceNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @PostMapping("/races")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<RaceDTO> addRace(@RequestBody RaceDTO raceDTO) {
        try {
            responseEntity = new ResponseEntity(raceServiceImpl.saveRace(raceDTO), HttpStatus.OK);
        } catch (RaceNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PutMapping("/races")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<RaceDTO> updateRace(@RequestBody RaceDTO raceDTO) {
        try {
            responseEntity = new ResponseEntity(raceServiceImpl.saveRace(raceDTO), HttpStatus.OK);
        } catch (RaceNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @DeleteMapping("/races/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<RaceDTO> deletRace(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(raceServiceImpl.deleteRace(id), HttpStatus.OK);
        } catch (RaceNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }
}

package dnd.dndproject.controller;

import dnd.dndproject.dto.PetDTO;
import dnd.dndproject.exception.PetNotFoundException;
import dnd.dndproject.service.impl.PetServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PetController {
    @Autowired
    private PetServiceImpl petServiceImpl;
    ResponseEntity responseEntity;

    @GetMapping("/pets")
    public ResponseEntity<List<PetDTO>> showPets() {
        return new ResponseEntity<>(petServiceImpl
                .getAllPets(),
                HttpStatus.OK);
    }

    @GetMapping("/pets/{id}")
    public ResponseEntity<PetDTO> getPetById(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(petServiceImpl.getPetById(id), HttpStatus.OK);
        } catch (PetNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @PostMapping("/pets")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PetDTO> addPet(@RequestBody PetDTO petDTO) {
        try {
            responseEntity = new ResponseEntity(petServiceImpl.savePet(petDTO), HttpStatus.OK);
        } catch (PetNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PutMapping("/pets")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PetDTO> updatePet(@RequestBody PetDTO petDTO) {
        try {
            responseEntity = new ResponseEntity(petServiceImpl.savePet(petDTO), HttpStatus.OK);
        } catch (PetNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @DeleteMapping("/pets/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PetDTO> deletePet(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(petServiceImpl.deletePet(id), HttpStatus.OK);
        } catch (PetNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }
}


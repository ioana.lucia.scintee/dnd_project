package dnd.dndproject.controller;

import dnd.dndproject.dto.CampaignDTO;
import dnd.dndproject.dto.CharacterDTO;
import dnd.dndproject.exception.CharacterNotFoundException;
import dnd.dndproject.service.impl.CharacterServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CharacterController {
    @Autowired
    private CharacterServiceImpl characterServiceImpl;

    ResponseEntity responseEntity;


    @GetMapping("/characters")
    public ResponseEntity<List<CharacterDTO>> showCharacters() {
        return new ResponseEntity<>(characterServiceImpl
                .getAllCharacters(),
                HttpStatus.OK);
    }

    @GetMapping("/characters/{id}")
    public ResponseEntity<CharacterDTO> getCampaignById(@PathVariable Long id) {
        try{
            responseEntity = new ResponseEntity(characterServiceImpl.getCharacterById(id), HttpStatus.OK);
        } catch (CharacterNotFoundException e){
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @PostMapping("/characters")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CharacterDTO>addCampaign(@RequestBody CharacterDTO characterDTO) {
        try {
            responseEntity = new ResponseEntity(characterServiceImpl.saveCharacter(characterDTO), HttpStatus.OK);
        } catch (CharacterNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PutMapping("/characters")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CampaignDTO> updateCharacter(@RequestBody CharacterDTO characterDTO) {
        try {
            responseEntity = new ResponseEntity(characterServiceImpl.saveCharacter(characterDTO), HttpStatus.OK);
        } catch (CharacterNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @DeleteMapping("/characters/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CharacterDTO> deleteCharacter(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(characterServiceImpl.deleteCharacter(id), HttpStatus.OK);
        } catch (CharacterNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }
}

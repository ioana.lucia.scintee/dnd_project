package dnd.dndproject.controller;

import dnd.dndproject.dto.CampaignDTO;
import dnd.dndproject.exception.CampaignNotFoundException;
import dnd.dndproject.service.impl.CampaignServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CampaignController {
    @Autowired
    private CampaignServiceImpl campaignServiceImpl;
    ResponseEntity responseEntity;


    @GetMapping("/campaigns")
    public ResponseEntity<List<CampaignDTO>> showCampaigns() {
        return new ResponseEntity<>(campaignServiceImpl
                .getAllCampaigns(),
                HttpStatus.OK);
    }

    @GetMapping("/campaigns/{id}")
    public ResponseEntity<CampaignDTO> getCampaignById(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(campaignServiceImpl.getCampaignById(id), HttpStatus.OK);
        } catch (CampaignNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @PostMapping("/campaigns")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CampaignDTO> addCampaign(@RequestBody CampaignDTO campaignDTO) {
        try {
            responseEntity = new ResponseEntity(campaignServiceImpl.saveCampaign(campaignDTO), HttpStatus.OK);
        } catch (CampaignNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PutMapping("/campaigns")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CampaignDTO> updateCampaign(@RequestBody CampaignDTO campaignDTO) {
        try {
            responseEntity = new ResponseEntity(campaignServiceImpl.saveCampaign(campaignDTO), HttpStatus.OK);
        } catch (CampaignNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @DeleteMapping("/campaigns/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CampaignDTO> deleteCampaign(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(campaignServiceImpl.deleteCampaign(id), HttpStatus.OK);
        } catch (CampaignNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }
}


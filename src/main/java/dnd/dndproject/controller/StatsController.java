package dnd.dndproject.controller;

import dnd.dndproject.dto.StatsDTO;
import dnd.dndproject.exception.StatsNotFoundException;
import dnd.dndproject.service.impl.StatsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
public class StatsController {
    @Autowired
    private StatsServiceImpl statsServiceImpl;
    private ResponseEntity responseEntity;


    @GetMapping("/stats")
    public ResponseEntity<List<StatsDTO>> showStats() {
        return new ResponseEntity<>(statsServiceImpl
                .getAllStats(),
                HttpStatus.OK);
    }

    @GetMapping("/stats/{id}")
    public ResponseEntity<StatsDTO> getStatsById(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(statsServiceImpl.getStatsById(id), HttpStatus.OK);
        } catch (StatsNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @PostMapping("/stats")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<StatsDTO> addStats(@RequestBody StatsDTO statsDTO) {
        try {
            responseEntity = new ResponseEntity(statsServiceImpl.saveStats(statsDTO), HttpStatus.OK);
        } catch (StatsNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PutMapping("/stats")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<StatsDTO> updateStats(@RequestBody StatsDTO statsDTO) {
        try {
            responseEntity = new ResponseEntity(statsServiceImpl.saveStats(statsDTO), HttpStatus.OK);
        } catch (StatsNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @DeleteMapping("/stats/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<StatsDTO> deleteStats(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(statsServiceImpl.deleteStats(id), HttpStatus.OK);
        } catch (StatsNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

}

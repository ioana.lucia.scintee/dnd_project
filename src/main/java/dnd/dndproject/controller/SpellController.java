package dnd.dndproject.controller;

import dnd.dndproject.dto.SpellDTO;
import dnd.dndproject.exception.SpellNotFoundException;
import dnd.dndproject.service.impl.SpellServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class SpellController {
    @Autowired
    private SpellServiceImpl spellServiceImpl;
    private ResponseEntity responseEntity;


    @GetMapping("/spells")
    public ResponseEntity<List<SpellDTO>> showSpells() {
        return new ResponseEntity<>(spellServiceImpl
                .getAllSpells(),
                HttpStatus.OK);
    }

    @GetMapping("/spells/{id}")
    public ResponseEntity<SpellDTO> getSpellById(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(spellServiceImpl.getSpellById(id), HttpStatus.OK);
        } catch (SpellNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @PostMapping("/spells")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<SpellDTO> addSpell(@RequestBody SpellDTO spellDTO) {
        try {
            responseEntity = new ResponseEntity(spellServiceImpl.saveSpell(spellDTO), HttpStatus.OK);
        } catch (SpellNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PutMapping("/spells")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<SpellDTO> updateSpell(@RequestBody SpellDTO spellDTO) {
        try {
            responseEntity = new ResponseEntity(spellServiceImpl.saveSpell(spellDTO), HttpStatus.OK);
        } catch (SpellNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @DeleteMapping("/spells/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<SpellDTO> deleteSpell(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(spellServiceImpl.deleteSpell(id), HttpStatus.OK);
        } catch (SpellNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }
}


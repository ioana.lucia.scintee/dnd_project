package dnd.dndproject.controller;

import dnd.dndproject.dto.CharacterDetailsDTO;
import dnd.dndproject.exception.CharacterDetailsNotFoundException;
import dnd.dndproject.service.impl.CharacterDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CharacterDetailsController {
    @Autowired
    private CharacterDetailsServiceImpl characterDetailsServiceImpl;

    ResponseEntity responseEntity;


    @GetMapping("/details")
    public ResponseEntity<List<CharacterDetailsDTO>> showDetails() {
        return new ResponseEntity<>(characterDetailsServiceImpl
                .getAllDetails(),
                HttpStatus.OK);
    }

    @GetMapping("/details/{id}")
    public ResponseEntity<CharacterDetailsDTO> getDetailById(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(characterDetailsServiceImpl.getDetailById(id), HttpStatus.OK);
        } catch (CharacterDetailsNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }


    @PostMapping("/details")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CharacterDetailsDTO> addDetail(@RequestBody CharacterDetailsDTO characterDetailsDTO) {
        try {
            responseEntity = new ResponseEntity(characterDetailsServiceImpl.saveDetail(characterDetailsDTO), HttpStatus.OK);
        } catch (CharacterDetailsNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PutMapping("/details")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CharacterDetailsDTO> updateDetail(@RequestBody CharacterDetailsDTO characterDetailsDTO) {
        try {
            responseEntity = new ResponseEntity(characterDetailsServiceImpl.saveDetail(characterDetailsDTO), HttpStatus.OK);
        } catch (CharacterDetailsNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @DeleteMapping("/details/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CharacterDetailsDTO> deleteClass(@PathVariable Long id) {
        try {
            responseEntity = new ResponseEntity(characterDetailsServiceImpl.deleteDetail(id), HttpStatus.OK);
        } catch (CharacterDetailsNotFoundException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }
}
